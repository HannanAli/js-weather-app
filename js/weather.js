
let allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Headers', "*");
    next();
};

// Initializing a class definition
class Weather {
    constructor(city) {
        this.city = city;
        this.app_id = '36909fea4068752f0b67970c195c7b01';
    }

    async getWeather() {
        const resData = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&appid=${this.app_id}`);
        const data = await resData.json();
        return data;
    }
}











